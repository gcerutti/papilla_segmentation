# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput
# #}
# add your imports before the next gnomon tag

import numpy as np

from timagetk import TissueImage3D
from timagetk.components.labelled_image import relabel_from_mapping

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='0.1.0', coreversion='1.0.0')
@cellImageInput(attr='tissue', data_plugin='gnomonCellImageDataTissueImage')
@cellImageOutput(attr='out_tissue', data_plugin='gnomonCellImageDataTissueImage')
class centralCellExtraction(gnomon.core.gnomonAbstractFormAlgorithm):
    """Keep only the most central cell

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['volume_weight'] = d_real("Volume weight", 1, 0, 10, 2, "How much to take the cell volume into account to determine the central cell")

        self.tissue = {}
        self.out_tissue = {}

    def run(self):
        self.out_tissue = {}
        for time in self.tissue.keys():
            tissue = self.tissue[time]
            # #}
            # implement the run method

            out_tissue = TissueImage3D(tissue.get_array(), voxelsize=tissue.voxelsize, not_a_label=0, background=1)

            cell_centers = out_tissue.cells.barycenter(real=True)
            cell_volumes = out_tissue.cells.volume(real=True)

            image_center = np.array(out_tissue.extent)[::-1]/2
            print(cell_centers, image_center)
            dim_weights = np.array([1., 0.5, 0.5])
            cell_center_distances = {
                # c: np.linalg.norm(cell_centers[c] - image_center)
                c: np.linalg.norm((cell_centers[c] - image_center)*dim_weights)
                for c in cell_centers
            }

            cell_scores = {
            	c: np.exp(-np.power(cell_center_distances[c]/np.linalg.norm(image_center), 2)) + self['volume_weight']*cell_volumes[c]/np.prod(out_tissue.extent)
                for c in cell_centers
            }
            central_cell = list(cell_scores.keys())[np.argmax(list(cell_scores.values()))]
            out_tissue = relabel_from_mapping(out_tissue, {cid: 1 if cid!=central_cell else 2 for cid in out_tissue.cell_ids()}, clear_unmapped=False)
            out_tissue = TissueImage3D(out_tissue, not_a_label=0, background=1)

            self.out_tissue[time] = out_tissue
