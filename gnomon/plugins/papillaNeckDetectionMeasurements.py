# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput, dataFrameOutput
# #}
# add your imports before the next gnomon tag

import logging

from tqdm import tqdm

import numpy as np
import pandas as pd
import scipy.ndimage as nd
from scipy.signal import argrelextrema

from timagetk import TissueImage3D


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='0.1.0', coreversion='1.0.0', name="Papilla Neck Detection")
@cellImageInput(attr='seg_img', data_plugin='gnomonCellImageDataTissueImage')
@dataFrameOutput(attr='neck_df', data_plugin='gnomonDataFrameDataPandas')
@cellImageOutput(attr='neck_img', data_plugin='gnomonCellImageDataTissueImage')
class papillaNeckDetectionMeasurements(gnomon.core.gnomonAbstractFormAlgorithm):
    """Detect the papilla neck and measure its section area

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'skeleton_resolution': d_real("Skeleton Resolution", 0.5, 0.1, 5., 1, "Characteristic length of the skeleton segments"),
            'skeleton_sigma': d_real("Skeleton Sigma", 1., 0., 10., 1, "Standard deviation of the Gaussian function used to compute the papilla skeleton (in µm)"),
            'detection_sigma': d_real("Detection Sigma", 3., 0., 10., 1, "Standard deviation of the smoothing used for neck detection (in µm)")
        }

        self.seg_img = {}
        self.neck_df = {}
        self.neck_img = {}

    def run(self):
        self.neck_df = {}
        self.neck_img = {}
        for time in self.seg_img.keys():
            seg_img = self.seg_img[time]
            # #}
            # implement the run method

            papilla_points = (np.transpose(np.where(seg_img == 2))*np.array(seg_img.voxelsize))[:, ::-1]

            # main papilla axis
            papilla_covariance = np.cov(papilla_points, rowvar=False)
            e_val, e_vec = np.linalg.eig(papilla_covariance)
            papilla_axis = e_vec[:, np.argmax(np.abs(e_val))]
            if papilla_axis[1] < 0:
                papilla_axis *= -1

            # papilla skeleton
            papilla_point_abscissa = np.einsum("...j,...ij->...i", papilla_axis, papilla_points)
            papilla_tip_abscissa = papilla_point_abscissa.min()
            papilla_point_abscissa = papilla_point_abscissa - papilla_tip_abscissa
            a_min = 0 # papilla_point_abscissa.min()
            a_max = 10*np.round(papilla_point_abscissa.max()/10)

            abscissa_range = np.linspace(a_min, a_max, int(np.round((a_max-a_min)/self["skeleton_resolution"]))+1)

            papilla_skeleton = []
            for a in tqdm(abscissa_range, unit="skeleton point"):
                abscissa_distances = np.abs(papilla_point_abscissa - a)
                abscissa_weights = np.exp(-np.square(abscissa_distances/self['skeleton_sigma']))
                papilla_skeleton += [(abscissa_weights[:, np.newaxis]*papilla_points).sum(axis=0) / abscissa_weights[:, np.newaxis].sum(axis=0)]
            papilla_skeleton = np.array(papilla_skeleton)
            # papilla_skeleton = np.transpose([nd.gaussian_filter1d(p, sigma=self['skeleton_sigma']/self["skeleton_resolution"]) for p in papilla_skeleton.T])
            papilla_skeleton_abscissa = np.einsum("...j,...ij->...i", papilla_axis, papilla_skeleton) - papilla_tip_abscissa

            # papilla skeleton bases
            papilla_skeleton_edge_vectors = papilla_skeleton[1:] - papilla_skeleton[:-1]
            papilla_skeleton_edge_vectors /= np.linalg.norm(papilla_skeleton_edge_vectors, axis=-1)[:, np.newaxis]

            papilla_skeleton_vectors = np.zeros_like(papilla_skeleton)
            papilla_skeleton_vectors[0] = papilla_skeleton_edge_vectors[0]
            papilla_skeleton_vectors[1:-1] = np.mean([papilla_skeleton_edge_vectors[1:], papilla_skeleton_edge_vectors[:-1]], axis=0)
            papilla_skeleton_vectors[-1] = papilla_skeleton_edge_vectors[-1]

            papilla_skeleton_edge_normals = np.cross(papilla_skeleton_edge_vectors, [0, 0, 1])
            papilla_skeleton_edge_normals /= np.linalg.norm(papilla_skeleton_edge_normals, axis=-1)[:, np.newaxis]

            papilla_skeleton_normals = np.zeros_like(papilla_skeleton)
            papilla_skeleton_normals[0] = papilla_skeleton_edge_normals[0]
            papilla_skeleton_normals[1:-1] = np.mean([papilla_skeleton_edge_normals[1:], papilla_skeleton_edge_normals[:-1]], axis=0)
            papilla_skeleton_normals[-1] = papilla_skeleton_edge_normals[-1]

            papilla_skeleton_z_axes = np.cross(papilla_skeleton_normals, papilla_skeleton_vectors)

            # papilla distance profiles
            papilla_mean_radial_distances = []
            papilla_top_radial_distances = []
            papilla_max_radial_distances = []
            for s, v in tqdm(zip(papilla_skeleton, papilla_skeleton_vectors), unit="skeleton point", total=len(papilla_skeleton)):
                skeleton_vectors = papilla_points - s
                skeleton_abscissa_distances = np.einsum("...j,...ij->...i", v, skeleton_vectors)
                skeleton_abscissa_weights = np.exp(-np.square(skeleton_abscissa_distances/self['skeleton_sigma']))

                skeleton_vectors -= skeleton_abscissa_distances[:, np.newaxis]*v
                skeleton_radial_distances = np.linalg.norm(skeleton_vectors, axis=-1)

                skeleton_radial_distances[skeleton_abscissa_weights<0.01] = np.nan
                papilla_mean_radial_distances += [np.nanmean(skeleton_radial_distances)]
                papilla_top_radial_distances += [np.nanpercentile(skeleton_radial_distances, 90)]
                papilla_max_radial_distances += [np.nanpercentile(skeleton_radial_distances, 99)]

            # neck detection
            extremum_sigma = self['detection_sigma']/self["skeleton_resolution"]

            # head_maxima = argrelextrema(nd.gaussian_filter(papilla_mean_radial_distances, sigma=extremum_sigma, mode='constant', cval=0), np.greater)[0]
            # head_index = head_maxima[0]

            neck_minima = argrelextrema(nd.gaussian_filter(papilla_mean_radial_distances, sigma=extremum_sigma, mode='constant', cval=0), np.less)[0]
            neck_index = neck_minima[0]

            neck_center = papilla_skeleton[neck_index]
            neck_abscissa = papilla_skeleton_abscissa[neck_index]
            neck_vector = papilla_skeleton_vectors[neck_index]
            neck_normal = papilla_skeleton_normals[neck_index]
            neck_z_axis = papilla_skeleton_z_axes[neck_index]

            # neck XZ slice
            neck_y = np.round(neck_center[1]/seg_img.voxelsize[1]).astype(int)
            neck_slice = seg_img.get_array()[:, neck_y, :]

            neck_xz_area = (neck_slice==2).sum()*(seg_img.voxelsize[0]*seg_img.voxelsize[2])
            # neck_xz_radius = np.sqrt(neck_xz_area/np.pi)
            neck_xz_width = (neck_slice==2).max(axis=0).sum()*seg_img.voxelsize[2]
            neck_xz_height = (neck_slice==2).max(axis=1).sum()*seg_img.voxelsize[0]
            # neck_xz_ellipse_area = (np.pi*neck_xz_width*neck_xz_height)/4

            # neck image slice
            xx, yy = np.meshgrid(np.linspace(-10, 10, int(np.round(20/seg_img.voxelsize[2]))+1), np.linspace(-10, 10, int(np.round(20/seg_img.voxelsize[0]))+1))

            _xx = xx[:, :, np.newaxis] * neck_normal[np.newaxis, np.newaxis]
            _yy = yy[:, :, np.newaxis] * neck_z_axis[np.newaxis, np.newaxis]
            grid_points = neck_center + _xx + _yy

            grid_coords = grid_points[:, :, ::-1] / np.array(seg_img.voxelsize)
            grid_coords = np.round(grid_coords).astype(int)
            outside_mask = np.logical_or(np.any(grid_coords<0, axis=-1), np.any(grid_coords>np.array(seg_img.shape)-1, axis=-1))
            grid_coords = np.maximum(0, np.minimum(grid_coords, np.array(seg_img.shape)-1))

            neck_img_2d = seg_img[tuple(np.transpose(np.concatenate(grid_coords)))].reshape(xx.shape)
            neck_img_2d[outside_mask] = 0

            neck_img = TissueImage3D(
                neck_img_2d.get_array()[np.newaxis],
                voxelsize=(1.0, seg_img.voxelsize[0], seg_img.voxelsize[2]),
                not_a_label=0, background=1
            )
            self.neck_img[time] = neck_img

            # neck measurements
            neck_section_area = (neck_img_2d.get_array()==2).sum()*(seg_img.voxelsize[0]*seg_img.voxelsize[2])
            # neck_section_radius = np.sqrt(neck_section_area/np.pi)

            neck_width = (neck_img_2d.get_array()==2).max(axis=0).sum()*seg_img.voxelsize[2]
            neck_height = (neck_img_2d.get_array()==2).max(axis=1).sum()*seg_img.voxelsize[0]
            # neck_ellipse_area = (np.pi*neck_width*neck_height)/4

            neck_data = {
                'z_voxelsize': [np.round(seg_img.voxelsize[0], 3)],
                'neck_y': [neck_y],
                'neck_xz_area': [neck_xz_area],
                'neck_xz_width': [neck_xz_width],
                'neck_xz_height': [neck_xz_height],
                'neck_abscissa': [neck_abscissa],
                'neck_area': [neck_section_area],
                'neck_width': [neck_width],
                'neck_height': [neck_height],
            }
            neck_df = pd.DataFrame(neck_data)

            self.neck_df[time] = neck_df
