# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import binaryImageInput, binaryImageOutput
# #}
# add your imports before the next gnomon tag

import numpy as np
import scipy.ndimage as nd

from timagetk import SpatialImage

def spherical_structuring_element(radius=1.0, voxelsize=(1., 1., 1.)):
    neighborhood = np.array(np.ceil(radius/np.array(voxelsize)),int)
    structuring_element = np.zeros(tuple(2*neighborhood+1),np.uint8)

    neighborhood_coords = np.mgrid[-neighborhood[0]:neighborhood[0]+1,-neighborhood[1]:neighborhood[1]+1,-neighborhood[2]:neighborhood[2]+1]
    neighborhood_coords = np.concatenate(np.concatenate(np.transpose(neighborhood_coords,(1,2,3,0)))) + neighborhood
    neighborhood_coords = np.unique(neighborhood_coords,axis=0)

    neighborhood_distance = np.linalg.norm(neighborhood_coords*voxelsize - neighborhood*voxelsize,axis=1)
    neighborhood_coords = neighborhood_coords[neighborhood_distance<=radius]
    neighborhood_coords = tuple(np.transpose(neighborhood_coords))
    structuring_element[neighborhood_coords] = 1

    return structuring_element

# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='0.1.0', coreversion='1.0.0')
@binaryImageInput(attr='img', data_plugin='binaryImageDataSpatialImage')
@binaryImageOutput(attr='out_img', data_plugin='binaryImageDataSpatialImage')
class binaryImageMorphoMat(gnomon.core.gnomonAbstractFormAlgorithm):
    """Perform a morphological operation on a binary image

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['operation'] = d_inliststring('Operation', 'erosion', ['erosion', 'dilation', 'opening', 'closing'], "Type of operation to apply")
        self._parameters['radius'] = d_real('Radius', 1, 0, 10, 1, "Radius of the structuring spherical element")
        self._parameters['iterations'] = d_int('Iterations', 1, 0, 10, "Number of iterations")
        self._parameters['fill_holes'] = d_bool('Fill holes', True, "Whether to fill holes in the image after the operation")
        self._parameters['largest_component'] = d_bool('Largest component', True, "Whether to keep only the largest component after the operation")

        self._parameter_groups = {}
        for parameter_name in ['fill_holes', 'largest_component']:
            self._parameter_groups[parameter_name] = 'postprocessing'

        self.img = {}
        self.out_img = {}

    def run(self):
        self.out_img = {}
        for time in self.img.keys():
            img = self.img[time]
            # #}
            # implement the run method

            structure = spherical_structuring_element(self['radius'], voxelsize=img.voxelsize)

            img_array = img.get_array()
            operation_func = {
                "erosion": nd.binary_erosion,
                "dilation": nd.binary_dilation,
                "opening": nd.binary_opening,
                "closing": nd.binary_closing,
            }
            img_array = operation_func[self['operation']](img_array, structure=structure, iterations=self['iterations'])

            if self['fill_holes']:
                img_array = nd.binary_fill_holes(img_array)

            if self['largest_component']:
                component_img, n_components = nd.label(img_array)
                components = np.arange(n_components)+1
                component_sizes = nd.sum(np.ones_like(img_array), component_img, index=components)
                largest_component = components[np.argmax(component_sizes)]
                img_array = component_img == largest_component

            out_img = SpatialImage(img_array.astype(bool), voxelsize=img.voxelsize)
            self.out_img[time] = out_img
