# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, cellImageOutput, dataFrameOutput, imageInput, imageOutput
# #}
# add your imports before the next gnomon tag

import logging

import numpy as np
import pandas as pd

from timagetk import SpatialImage, MultiChannelImage, TissueImage3D


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='0.1.0', coreversion='1.0.0', name="Papilla Neck")
@imageInput(attr='img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageInput(attr='seg_img', data_plugin='gnomonCellImageDataTissueImage')
@dataFrameOutput(attr='neck_df', data_plugin='gnomonDataFrameDataPandas')
@imageOutput(attr='neck_img', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput(attr='neck_seg_img', data_plugin='gnomonCellImageDataTissueImage')
class papillaNeckXZMeasurements(gnomon.core.gnomonAbstractFormAlgorithm):
    """Measure the neck XZ slice area

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'y_slice': d_int("Y Slice", 0, 0, 100, "Y slice in which to make papilla measurements")
        }

        self.img = {}
        self.seg_img = {}
        self.neck_df = {}
        self.neck_img = {}
        self.neck_seg_img = {}

    def refreshParameters(self):
        if len(self.seg_img) > 0:
            seg_img = list(self.seg_img.values())[0]
            self._parameters['y_slice'].setMin(0)
            self._parameters['y_slice'].setMax(seg_img.shape[1]-1)
            self._parameters['y_slice'].setValue((seg_img.shape[1]-1)//2)

    def run(self):
        self.neck_df = {}
        self.neck_img = {}
        self.neck_seg_img = {}
        for time in self.seg_img.keys():
            seg_img = self.seg_img[time]
            # #}
            # implement the run method

            if time in self.img:
                img = self.img[time]

            # neck XZ slice
            neck_y = self['y_slice']
            neck_seg_slice = seg_img.get_array()[:, neck_y, :]

            neck_xz_area = (neck_seg_slice==2).sum()*(seg_img.voxelsize[0]*seg_img.voxelsize[2])
            # neck_xz_radius = np.sqrt(neck_xz_area/np.pi)
            neck_xz_width = (neck_seg_slice==2).max(axis=0).sum()*seg_img.voxelsize[2]
            neck_xz_height = (neck_seg_slice==2).max(axis=1).sum()*seg_img.voxelsize[0]
            # neck_xz_ellipse_area = (np.pi*neck_xz_width*neck_xz_height)/4

            neck_seg_img = TissueImage3D(
                neck_seg_slice[np.newaxis],
                voxelsize=(1.0, seg_img.voxelsize[0], seg_img.voxelsize[2]),
                not_a_label=0, background=1
            )
            self.neck_seg_img[time] = neck_seg_img

            if time in self.img:
                neck_img = MultiChannelImage({
                    c: SpatialImage(
                        img[c].get_array()[:, neck_y, :][np.newaxis],
                        voxelsize=(1.0, seg_img.voxelsize[0], seg_img.voxelsize[2])
                    )
                    for c in img.channel_names
                })
                self.neck_img[time] = neck_img

            neck_data = {
                'z_voxelsize': [np.round(seg_img.voxelsize[0], 3)],
                'neck_y': [neck_y],
                'neck_xz_area': [neck_xz_area],
                'neck_xz_width': [neck_xz_width],
                'neck_xz_height': [neck_xz_height]
            }
            neck_df = pd.DataFrame(neck_data)

            self.neck_df[time] = neck_df
