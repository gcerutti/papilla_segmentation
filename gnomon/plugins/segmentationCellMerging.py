# {# gnomon, plugin.imports
# do not modify, any code after the gnomon tag will be overwritten
from dtkcore import d_bool, d_int, d_real, d_inliststring, d_inliststringlist

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellImageInput, imageInput, cellImageOutput
# #}
# add your imports before the next gnomon tag

import logging

import numpy as np

from skimage.filters.thresholding import threshold_otsu

from timagetk import TissueImage3D
from timagetk.algorithms.topological_elements import topological_elements_extraction3D


# {# gnomon, plugin.class
# do not modify, any code after the gnomon tag will be overwritten
@algorithmPlugin(version='0.1.0', coreversion='1.0.0')
@cellImageInput(attr='in_seg_img', data_plugin='gnomonCellImageDataTissueImage')
@imageInput(attr='img_dict', data_plugin='gnomonImageDataMultiChannelImage')
@cellImageOutput(attr='seg_img', data_plugin='gnomonCellImageDataTissueImage')
class segmentationCellMerging(gnomon.core.gnomonAbstractFormAlgorithm):
    """Merge cells with low average intensity interfaces

    """

    def __init__(self):
        super().__init__()

        self._parameters = {
            'channel': d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm"),
            'threshold': d_int("Merging threshold", 10000, 0, 65535, "Average interface intensity threshold below which cells will be merged"),
            'min_area': d_int("Minimum area", 1e2, 0, 1e5, "Minimun interface area below which cells can not be merged")
        }

        self.in_seg_img = {}
        self.img_dict = {}
        self.seg_img = {}

    def refreshParameters(self):
        if len(self.img_dict) > 0:
            img_dict = list(self.img_dict.values())[0]
            if len(img_dict) == 1:
                if 'channel' in self._parameters.keys():
                    del self._parameters['channel']
            else:
                if 'channel' not in self._parameters.keys():
                    self._parameters['channel'] = d_inliststring("Channel", "", [""], "Channel on which to apply the algorithm")
                    self.connectParameter('channel')
                channel = self['channel']
                self._parameters['channel'].setValues(list(img_dict.keys()))
                if channel in img_dict.keys():
                    self._parameters['channel'].setValue(channel)
                else:
                    self._parameters['channel'].setValue(list(img_dict.keys())[0])

            if len(img_dict) == 1:
                image = list(img_dict.values())[0]
            else:
                image = img_dict[self['channel']]
            if image.dtype == np.uint8:
                self._parameters['threshold'].setMax(255)
            elif image.dtype == np.uint16:
                self._parameters['threshold'].setMax(65535)
            self._parameters['threshold'].setValue(threshold_otsu(image.get_array()))
            
    def run(self):
        self.seg_img = {}
        for time in self.in_seg_img.keys():
            in_seg_img = self.in_seg_img[time]
            img_dict = self.img_dict[time]
            # #}
            # implement the run method

            seg_img = TissueImage3D(in_seg_img.get_array(), voxelsize=in_seg_img.voxelsize, not_a_label=0, background=1)

            if 'channel' in self._parameters:
                img = img_dict[self['channel']]
            else:
                img = list(img_dict.values())[0]
                
            cells_merged = len(seg_img.labels())
            while cells_merged > 0:
                surfels = topological_elements_extraction3D(seg_img, elem_order=[2])[2]

                wall_intensities = {}
                for w, wall_surfels in surfels.items():
                    wall_coords = np.array(list(np.floor(wall_surfels)) + list(np.ceil(wall_surfels)))
                    wall_coords = np.maximum(0, np.minimum(wall_coords, np.array(img.shape)-1)).astype(int)
                    wall_coords = np.unique(wall_coords, axis=0)
                    wall_intensities[w] = img[tuple(np.transpose(wall_coords))]

                wall_mean_intensities = {w: np.mean(wall_intensities[w]) for w in surfels}
                logging.debug(wall_mean_intensities)

                wall_areas = seg_img.walls.area()

                cells_to_merge = [w for w in surfels if wall_mean_intensities[w] < self['threshold']]
                cells_to_merge = [w for w in cells_to_merge if wall_areas[w] >= self['min_area']]
                cells_to_merge = np.array(cells_to_merge)
                merge_intensities = [wall_mean_intensities[tuple(w)] for w in cells_to_merge]

                merge_intensity_sorting = np.argsort(merge_intensities)
                cells_to_merge = cells_to_merge[merge_intensity_sorting]

                cells_merged = 0
                affected_cells = set()
                logging.info(f"--> Merging cells...")
                relabel_dict = {}
                for c, n in cells_to_merge:
                    if c not in affected_cells and n not in affected_cells:
                        logging.info(f"  --> Merge cell {n} into cell {c}")
                        relabel_dict[n] = c
                        affected_cells |= {c, n}
                        cells_merged += 1

                if len(relabel_dict)>0:
                    seg_img = seg_img.relabelling_cells_from_mapping(relabel_dict)
                logging.info(f"<-- Merged {cells_merged} cells! {len(seg_img.labels())} cells remaining")

            self.seg_img[time] = seg_img
