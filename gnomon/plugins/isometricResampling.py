from copy import deepcopy
import numpy as np

from dtkcore import d_real

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import imageInput, imageOutput

from timagetk.algorithms.resample import isometric_resampling


@algorithmPlugin(version='0.1.0', coreversion='1.0.0')
@imageInput(attr='in_img', data_plugin='gnomonImageDataMultiChannelImage')
@imageOutput(attr='out_img', data_plugin='gnomonImageDataMultiChannelImage')
class isometricResampling(gnomon.core.gnomonAbstractFormAlgorithm):

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['voxelsize'] = d_real("Voxelsize", 1., 0.1, 5., 2, "Target voxelsize after resampling")

        self.in_img = {}
        self.out_img = {}

    def run(self):
        self.out_img = {}

        for time in self.in_img.keys():
            resampled_img = isometric_resampling(deepcopy(self.in_img[time]), method=self['voxelsize'])
            self.out_img[time] = resampled_img

