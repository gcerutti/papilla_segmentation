import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('cellImageReader')
    load_plugin_group('cellImageWriter')
    load_plugin_group('dataFrameWriter')
    load_plugin_group('imageFilter')
    load_plugin_group('imageReader')
    load_plugin_group('imageWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default=".")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : data/]', default=None)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    parser.add_argument('-y', '--y-slice', help='Y coordinate of the slice used to make neck measurements', required=True, type=int)
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path
    filename = args.filename

    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "data/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names # ["mCher", "EGFP"]

    y_slice = args.y_slice

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    path = f"{microscopy_dirname}/{filename}.czi"
    tif_file = False
    if not os.path.exists(path):
        path = f"{microscopy_dirname}/{filename}.tif"
        tif_file = True
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1", "ChS1"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    segmentation_reader = gnomon.core.cellImageReader_pluginFactory().create("cellImageReaderTimagetk")
    path = f"{output_dirname}/{filename}/segmentation/{filename}_segmented_papilla.tif"
    segmentation_reader.setPath(path)
    segmentation_reader.run()

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "papillaNeckXZMeasurements"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    neck_measurements = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    neck_measurements.setInputImage(rename.output())
    neck_measurements.setInputCellImage(segmentation_reader.cellImage())
    neck_measurements.refreshParameters()
    neck_measurements.setParameter("y_slice", y_slice)
    neck_measurements.run()

    if not os.path.exists(f"{output_dirname}/{filename}/papilla_data"):
        os.makedirs(f"{output_dirname}/{filename}/papilla_data")
    writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    writer.setDataFrame(neck_measurements.outputDataFrame())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck_XZ_slice_data.csv")
    writer.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(neck_measurements.outputCellImage())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck_XZ_slice_seg.tif")
    writer.run()

    writer = gnomon.core.imageWriter_pluginFactory().create("gnomonImageWriter")
    writer.setImage(neck_measurements.outputImage())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck_XZ_slice.tif")
    writer.run()

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "papillaNeckDetectionMeasurements"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    neck_detection = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    neck_detection.setInputCellImage(segmentation_reader.cellImage())
    neck_detection.refreshParameters()
    # neck_detection.setParameter("skeleton_resolution", 0.25)
    neck_detection.setParameter("skeleton_resolution", 1)
    neck_detection.setParameter("skeleton_sigma", 1.)
    neck_detection.setParameter("detection_sigma", 2.)
    neck_detection.run()

    if not os.path.exists(f"{output_dirname}/{filename}/papilla_data"):
        os.makedirs(f"{output_dirname}/{filename}/papilla_data")
    writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    writer.setDataFrame(neck_detection.outputDataFrame())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck_data.csv")
    writer.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(neck_detection.outputCellImage())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck.tif")
    writer.run()
