import os
import importlib
import argparse

import gnomon.core
import gnomon.visualization

from dtkcore import array_real_2

from gnomon.utils.gnomonPlugin import load_plugin_group
from gnomon.utils.views import gnomonStandaloneVtkView, gnomonStandaloneMplView


def initialize_plugins():
    load_plugin_group('binaryImageFromImage')
    load_plugin_group('binaryImageVtkVisualization')
    load_plugin_group('binaryImageWriter')
    load_plugin_group('dataFrameWriter')
    load_plugin_group('imageReader')
    load_plugin_group('imageFilter')
    load_plugin_group('imageVtkVisualization')
    load_plugin_group('imageWriter')
    load_plugin_group('cellImageFromImage')
    load_plugin_group('cellImageVtkVisualization')
    load_plugin_group('cellImageWriter')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--project-path', help='Path to the gnomon project directory', default=".")
    parser.add_argument('-f', '--filename', help='SAM image sequence identifier', required=True)
    parser.add_argument('-m', '--microscopy-directory', help='Relative path to CZI image directory [default : data/]', default=None)
    parser.add_argument('-r', '--results-directory', help='Relative path to directory storing the results [default : results/]', default=None)
    parser.add_argument('-c', '--channel-names', help='List of chanel names in the same order as in the microscopy image', nargs='+', type=str, required=True)
    args = parser.parse_args()

    initialize_plugins()

    project_path = args.project_path
    filename = args.filename
    microscopy_directory = args.microscopy_directory if args.microscopy_directory is not None else "data/"
    microscopy_dirname = f"{project_path}/{microscopy_directory}"
    channel_names = args.channel_names # ["mCher", "EGFP"]

    output_directory = args.results_directory if args.results_directory is not None else "results/"
    output_dirname = f"{project_path}/{output_directory}"
    if not os.path.exists(f"{output_dirname}/{filename}"):
        os.makedirs(f"{output_dirname}/{filename}")

    reader = gnomon.core.imageReader_pluginFactory().create("imageReaderTimagetk")
    path = f"{microscopy_dirname}/{filename}.czi"
    tif_file = False
    if not os.path.exists(path):
        path = f"{microscopy_dirname}/{filename}.tif"
        tif_file = True
    reader.setPath(path)
    reader.run()

    old_channel_names = reader.image()[0].channels() # ["Ch1", "ChS1"]

    rename = gnomon.core.imageFilter_pluginFactory().create("channelNamesEdit")
    rename.setInput(reader.image())
    rename.refreshParameters()
    for old_channel_name, channel_name in zip(old_channel_names, channel_names):
        rename.setParameter(old_channel_name, channel_name)
    rename.run()

    if not os.path.exists(f"{output_dirname}/{filename}/image"):
        os.makedirs(f"{output_dirname}/{filename}/image")
    channel_colormaps = {'EGFP': '0RGB_green', 'mCher': '0CMY_magenta'}
    view = gnomonStandaloneVtkView()
    for channel in channel_names:
        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", channel)
        visu.setParameter("colormap", channel_colormaps[channel])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.5)
        visu.setParameter("brightness", 1.5)
        visu.setParameter("contrast", 1.5)
        visu.update()
        visu.setVisible(True)

    view.updateBounds()
    view.setCameraXY(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/image/{filename}_image.png")

    # papilla segmentation
    segmentation = gnomon.core.cellImageFromImage_pluginFactory().create("autoSeededWatershedSegmentation")
    segmentation.setInput(rename.output())
    segmentation.refreshParameters()
    segmentation.setParameter("membrane_channel", "EGFP")
    # segmentation.setParameter("gaussian_sigma", 4)
    segmentation.setParameter("gaussian_sigma", 3) # *
    # segmentation.setParameter("seg_gaussian_sigma", 1.) # Actin
    segmentation.setParameter("seg_gaussian_sigma", 0.5) # *
    # segmentation.setParameter("seg_gaussian_sigma", 0.)
    # segmentation.setParameter("h_min", 200) # *
    segmentation.setParameter("h_min", 500)
    # segmentation.setParameter("h_min", 1000)
    segmentation.setParameter("max_volume", 5e4)
    segmentation.run()

    if not os.path.exists(f"{output_dirname}/{filename}/segmentation"):
        os.makedirs(f"{output_dirname}/{filename}/segmentation")
    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(segmentation.output())
    writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation_raw.inr.gz")
    writer.run()

    view = gnomonStandaloneVtkView()
    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(segmentation.output())
    visu.refreshParameters()
    visu.update()

    visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
    visu.setView(view)
    visu.setImage(rename.output())
    visu.refreshParameters()
    visu.setParameter("channel", 'EGFP')
    visu.setParameter("colormap", channel_colormaps['EGFP'])
    visu.setParameter("show_histogram", False)
    visu.setParameter("opacity", 0.)
    visu.update()

    view.updateBounds()
    view.setCameraXY(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation_raw.png")

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "segmentationCellMerging"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    cell_merging = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    cell_merging.setInputCellImage(segmentation.output())
    cell_merging.setInputImage(rename.output())
    cell_merging.refreshParameters()
    cell_merging.setParameter("channel", "EGFP")
    # cell_merging.setParameter("threshold", 10000) # *
    cell_merging.setParameter("threshold", 15000)
    cell_merging.setParameter("min_area", 30.)
    cell_merging.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(cell_merging.outputCellImage())
    writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation.inr.gz")
    writer.run()

    view = gnomonStandaloneVtkView()
    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(cell_merging.outputCellImage())
    visu.refreshParameters()
    visu.update()

    visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
    visu.setView(view)
    visu.setImage(rename.output())
    visu.refreshParameters()
    visu.setParameter("channel", 'EGFP')
    visu.setParameter("colormap", channel_colormaps['EGFP'])
    visu.setParameter("show_histogram", False)
    visu.setParameter("opacity", 0.)
    visu.update()

    view.updateBounds()
    view.setCameraXY(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmentation.png")

    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "centralCellExtraction"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    central_cell = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    central_cell.setInputCellImage(cell_merging.outputCellImage())
    central_cell.refreshParameters()
    central_cell.setParameter("volume_weight", 0)
    central_cell.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(central_cell.outputCellImage())
    writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_segmented_papilla.tif")
    writer.run()

    # papilla visualization
    view = gnomonStandaloneVtkView()
    visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
    visu.setView(view)
    visu.setCellImage(central_cell.outputCellImage())
    visu.refreshParameters()
    visu.update()

    visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
    visu.setView(view)
    visu.setImage(rename.output())
    visu.refreshParameters()
    visu.setParameter("channel", 'EGFP')
    visu.setParameter("colormap", channel_colormaps['EGFP'])
    visu.setParameter("show_histogram", False)
    visu.setParameter("opacity", 0.2)
    visu.update()

    view.updateBounds()
    view.setCameraXY(True, True)
    view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmented_papilla.png")

    # papilla neck measurements
    gnomon.core.formAlgorithm_pluginFactory().clear()
    plugin_name = "papillaNeckDetectionMeasurements"
    plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
    importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

    neck_measurements = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
    neck_measurements.setInputCellImage(central_cell.outputCellImage())
    neck_measurements.refreshParameters()
    # neck_measurements.setParameter("skeleton_resolution", 0.25)
    neck_measurements.setParameter("skeleton_resolution", 1.)
    neck_measurements.setParameter("skeleton_sigma", 1.)
    neck_measurements.setParameter("detection_sigma", 2.)
    neck_measurements.run()

    if not os.path.exists(f"{output_dirname}/{filename}/papilla_data"):
        os.makedirs(f"{output_dirname}/{filename}/papilla_data")
    writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
    writer.setDataFrame(neck_measurements.outputDataFrame())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck_data.csv")
    writer.run()

    writer = gnomon.core.cellImageWriter_pluginFactory().create("cellImageWriterTissueImage")
    writer.setCellImage(neck_measurements.outputCellImage())
    writer.setPath(f"{output_dirname}/{filename}/papilla_data/{filename}_papilla_neck.tif")
    writer.run()

    # pollen segmentation
    if "mCher" in channel_names:
        smoothing = gnomon.core.imageFilter_pluginFactory().create("linearFilterTimagetk")
        smoothing.setInput(rename.output())
        smoothing.refreshParameters()
        smoothing.setParameter("method", "smoothing")
        smoothing.setParameter("gaussian_sigma", 1)
        smoothing.setParameter("channels", channel_names)
        smoothing.run()

        binarization = gnomon.core.binaryImageFromImage_pluginFactory().create("binarization")
        binarization.setInput(smoothing.output())
        binarization.refreshParameters()
        binarization.setParameter("channel", "mCher")
        binarization.setParameter("greater_or_lower", "greater >")
        # binarization.setParameter("threshold", 30000)
        # binarization.setParameter("threshold", 3000)
        binarization.setParameter("threshold", 0.8*binarization.parameters()['threshold'].value())
        binarization.run()

        gnomon.core.formAlgorithm_pluginFactory().clear()
        plugin_name = "binaryImageMorphoMat"
        plugin_path = f"{project_path}/gnomon/plugins/{plugin_name}.py"
        importlib.machinery.SourceFileLoader(plugin_name, plugin_path).load_module()

        binary_processing = gnomon.core.formAlgorithm_pluginFactory().create(plugin_name)
        binary_processing.setInputBinaryImage(binarization.output())
        binary_processing.refreshParameters()
        binary_processing.setParameter('operation', 'closing')
        binary_processing.setParameter('radius', 1.)
        binary_processing.setParameter('iterations', 3)
        binary_processing.setParameter('largest_component', True)
        binary_processing.run()

        writer = gnomon.core.binaryImageWriter_pluginFactory().create("binaryImageWriter")
        writer.setBinaryImage(binary_processing.outputBinaryImage())
        writer.setPath(f"{output_dirname}/{filename}/segmentation/{filename}_segmented_pollen.tif")
        writer.run()

        # pollen visualization
        view = gnomonStandaloneVtkView()
        visu = gnomon.visualization.binaryImageVtkVisualization_pluginFactory().create("binaryImageVisualization")
        visu.setView(view)
        visu.setBinaryImage(binary_processing.outputBinaryImage())
        visu.refreshParameters()
        visu.setParameter("color", 'darkorchid')
        visu.update()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", 'mCher')
        visu.setParameter("colormap", channel_colormaps['mCher'])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0.2)
        visu.update()

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmented_pollen.png")

        # superimpose visualization
        view = gnomonStandaloneVtkView()
        visu = gnomon.visualization.binaryImageVtkVisualization_pluginFactory().create("binaryImageVisualization")
        visu.setView(view)
        visu.setBinaryImage(binary_processing.outputBinaryImage())
        visu.refreshParameters()
        visu.setParameter("color", 'darkorchid')
        visu.update()

        visu = gnomon.visualization.cellImageVtkVisualization_pluginFactory().create("cellImageVtkVisualizationMarchingCubes")
        visu.setView(view)
        visu.setCellImage(central_cell.outputCellImage())
        visu.refreshParameters()
        visu.update()

        visu = gnomon.visualization.imageVtkVisualization_pluginFactory().create("imageChannelHistogram")
        visu.setView(view)
        visu.setImage(rename.output())
        visu.refreshParameters()
        visu.setParameter("channel", 'mCher')
        visu.setParameter("colormap", channel_colormaps['mCher'])
        visu.setParameter("show_histogram", False)
        visu.setParameter("opacity", 0)
        visu.update()

        view.updateBounds()
        view.setCameraXY(True, True)
        view.saveScreenshot(f"{output_dirname}/{filename}/segmentation/{filename}_segmented_pollen_papilla.png")
